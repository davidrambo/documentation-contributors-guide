include::ROOT:partial$attributes.adoc[]

= Fedora Documentation Team

image::docs-banner.jpg[Fedora Documentation Team logo with Fedora Trademark; book pages in the background.]

[abstract]
____
The Fedora Documentation Team is responsible for the management, curation, and publishing of the documentation hosted at https://docs.fedoraproject.org[docs.fedoraproject.org].
____

== What we do

* manage and curate the docs landing page
* maintain the Fedora Docs Website
* maintain the release notes
* maintain the documentation CI (Continuous Integration)
* maintain and update generic documentation
* curate the Quick Docs collection of documentation

== Who we are

The documentation team is a peer group with many members involved in very different ways. Especially important: You do not have to become a "member" to contribute to the documentation. Anyone can contribute!  So it's rather the other way around: if you contribute a text or a correction, you basically become a "peer". 

And by the way, no special skills are necessary to be able to contribute. There are many ways and options to constructively doing so.

If you are interested in creating and shaping valuable information or would like to share your own knowledge and experience, don't hesitate to join us. And don't be afraid that you will have to make a long-term commitment at the same time. We know many different ways and intensities of contributing. See the xref::organizational/charter.adoc[team charter] for information on how we are organized.

=== The Board
The Board's task is to coordinate and organize the work of the documentation team. Current members are

https://fedoraproject.org/wiki/User:Darknao[darknao]::
technical contributor
https://fedoraproject.org/wiki/User:Pbokoc[pbokoc]::
release note editor in charge and overall contributor
https://fedoraproject.org/wiki/User:Pboy[pboy]::
content writer and editor

Currently, we are a small but productive team. And we don’t necessarily want to be a small team in the long run. If you are interested in creating and shaping valuable information and are willing to make some commitment of regular work, please join us.

== Ongoing working projects

Migration of docs repositories::
We are currently in the process of moving all our documentaion from the pagure git repository to gitlab, to make use of several advanced features.
Lead: darknao

Improve the overall curation of Fedora docs::
A longer term work to better adapt the documentation to the current structure of the Fedora distribution and update it at the same time.
Lead: pboy

=== Some most recently finished efforts

* Implement a long missing search function across all docs
* Exclude old docs from search engines
* Streamline previous release note

=== Projects in the pipeline waiting to be taken up

Improving the contributors guide::
We need a more detailed description of all the options available to contribute. And we may need a more extensive description of the “Pull Request” workflow to get new or modified content into our repos.

Developing a style guide::
Currently, there is no style guide resulting in an inconsistent typography and unpleasant presentation of our docs.

Curation of the Quick Docs collection::
The articles in the Quick Docs section need some care and updating.

Exploring possible synergy effects of a cooperation between CentOS and Fedora documentation::
Both distribution share a lot of code and a large number of almost identical administrative routines. It should be possible to achieve improvements for both documentation projects by joint work and coordinating efforts.

[[find-docs]]
== Where to find Fedora docs team

Please contact us at any of our communication channels.

Fedora discussion forum::
https://discussion.fedoraproject.org/tag/docs-team

IRC channel::
#fedora-docs on Libera IRC

Matrix room::
#docs:fedora.im

Weekly IRC meeting::
Each Wednesday, 18:30 UTC at #fedora-meeting-1

== Have a look on our working infrastructure

We track work in https://gitlab.com/groups/fedora/docs/-/boards[GitLab].


== If you managed to read this far

Please contact us using one of the above channels. You are highly ‘suspect’ to become a very helpful and respected member of our team. Don’t hesitate.



