= Contributing to Quick Docs
 Hanku Lee; Peter Boy; Fedora Documentation Team
:revdate: 2023-03-19
:page-aliases: contributing-docs/contributions-quick-repo.adoc

[abstract]
Quick Docs is a collection of shorter how-to guide and more extensive tutorials for users of the Fedora Linux. We provide some tips and hints on contributing to Quick Docs.

== Types of content

Fedora Quick Docs consists of two types of articles.

=== How-to guides
Its characteristics are

- Intended to be useful when working with Fedora
- Problem and goal orientated
- It is usually a relatively short text no longer than 60,000 characters
- Comes usually as a step-by-step instruction to resolve a small, specific problem or task, without extensive explanation

=== Tutorials
Its characteristics are

- Intended to be useful when studying a software or a set thereof for a specific purpose in Fedora
- Characterized by learning new skills
- It is often a more extensive text longer than 80,000 characters
- Ensure comprehension of the features followed by walkthroughs of a software program

If your post does not fit into either category, consider an article under Administration Tools or on the Home page. The best place to ask is discussion.fedoraproject.org #docs.


== Who can contribute

**Everyone** can contribute and everyone is welcome! There are some minor and easy to meet requirements, though. The most important is a Fedora account having signed the https://fedoraproject.org/wiki/Legal:Fedora_Project_Contributor_Agreement[Fedora Project Contributor Agreement]. This is essential to protect Fedora from any potential licensing issues. Of course, some knowledge of the AsciiDOC markup language is helpful, but you can get along very well without it. This is especially true when correcting and revising existing articles. One can easily get into the existing flow.

An important fact is, _no special knowledge_ is required! You don't need to know anything about desktop publishing, complicated formatting tricks or anything like that.

The range of feasible and needed contributions is wide. Everyone can contribute an important element. Typical roles/tasks are;

- writer
- technical reviewer
- spell checking
- wording improvements
- QuickDoc organization
- revision of the articles

== How to find tasks

Whether it is broken link, outdated screenshots of graphical user interface, technical and grammatical error, every little contribution, no matter how small, helps Fedora Linux users.

=== While reading an article

The greatest opportunity to contribute comes from reading an article itself. Whenever you come across an error, an omission or unclear wording, you can contribute an improvement in passing. In the header of each article are links to 2 of the 3 tools for contributions.

.Links to contributing tools
image::contrib-quickdocs/easy-contrib.png[]

The rightmost button leads to a ticket system where you can point out the problem.

.Point out blunders you found
image::contrib-quickdocs/report-issue.png[]

The subject field already contains a link to the affected document. Don't change it. Please, provide a description of the issue and if possible provide a concise suggested modification, preferably cut&paste ready.

If you are not logged to pagure, a FAS login window will open first.

The second link opens a web editor, you can use to enter proposed modifications directly in the text.

.The web editor
image::contrib-quickdocs/report-issue.png[]

The next steps are explained in detail in the article xref:contributing-docs/tools-file-edit-pagure.adoc[How to edit files on the Pagure Web UI].

=== Self-assign issues

You can find and contribute to needed changes by following the https://pagure.io/fedora-docs/quick-docs/issues[Quick Docs Issue Board], where users share blunders in the documentation or suggestions for enhancing it.

If you find issue that matches your interest and expertise, on assignee on top right, press *take* link to assign issue to yourself.

.Assign issues
image::contrib-quickdocs/assign-issues.png[]

== How to contribute

Quick Docs uses the same CMS as all other Fedora Docs, so the tools are the same. You find detailed guides in the section Contribution Tools.

=== Pull Requests and review

In either case, the workflow follows the "pull request" or "merge request" model. Changes are not made in the existing, published text but in a side version. When the work is complete, a "merge request" is triggered. Members of the community will look at the changes and either start a discussion or accept the changes. They will then be merged into the published version. We thus follow the 4-eyes principle.

Pull Requests promote collaborative work flow between writers and reviewers where PR comments, edits and commits serve as review process.

The individual steps are largely automatic. Details are described in the respective tools.

=== Notification after PR

You probably want to track the further progress after the PR. That's what the Watch function is for.

Watch status in the middle of Quick Doc report has four options. Select one of the options from below.

- Watch Issues and PRs

- Watch Commits

- Watch Issues, PRs, and Commits

- Unwatch

Notification will go to your email as in user settings of Pagure.

.Notification options
image::contrib-quickdocs/watch-notification.png[]

If you do not hear from reviewers after your PR longer than a week, leave a comment in PR to follow up.


== What to pay attention to

=== Readability

- Write short sentences, structured into short paragraphs. Avoid the wall of text when creating content.

- Use clear structure and minimal formatting for better reading flow

=== Adherence to style guide

Instead of nitpicking consistency of tone and grammatical errors, reviewers encourage writers to read xref:contributing-docs/style-guide.adoc#prerequisites[The docs style guide]. If you want automated test, run vale linter locally xref:contributing-docs/tools-vale-linter.adoc#prerequisites[Check Your Documentation Using Vale].

=== Rule sets

Beyond style guide, here are a few rule sets contributors need to observe.

- The process Docs team treat bugs in documentation follows the same principle as code bugs where continuous integration, automated testing, and gradual improvements for documentation quality are adopted.

- All changes to Quick Docs must be done via Pull Requests from forked project. Quick Docs project does not support direct push to its upstream repo.

- If you fix contents about new features, releases or new content, test the application and process in a new system or freshly installed Virtual Machine with all updates applied.

- Do not use screenshot of terminal. Provide source code block of a particular language or shell commands.

- Run prose linter like vale locally before PR. Check this link:
xref:contributing-docs/tools-vale-linter.adoc#prerequisites[How to check documentation style with Vale]



== Article template

[source,asciidoc]
----
= ARTICLE_TITLE
AUTHOR_1; AUTHOR_2; AUTHOR_3
:revnumber: Fxy
:revdate: yyyy-mm-dd
:category: CATEGORY
:tags: TAG_01 TAG_02 TAG_03 ... TAG_n
//:page-aliases: OPTIONAL

// Optional  free form useful additional information as comment


[abstract]
------
Mission statement of 2-3 sentences
------

----

Notes:

// Title::
Authors line:: We would like to list the last 3 authors who worked on the article in terms of substance and content. Of coure, any contribution is welcome! But at this prominent position we do not want to include minor changes, such as spelling mistakes or individual wording corrections.
+
Alternatively use __The Fedora Documentation Team__

revnumber::
Use the release number preceded by 'F', e.g. F37, or a range of releases, e.g. F36,F37,F38 or F33-F37. Be as specific as possible and use 'all' only in exceptional cases

revdate:: The last date someone checked the content for category:: Select only one category from below. A category is mandatory! Categories (and tags) are the only way to retrieve an atricle.

tags:: one or more tags from list below
abstract:: 1-3 sentences describing content and goal. Avoid any redundancy, e.g. repeating the title

You can also xref:attachment$qd-template.adoc[download the template] to make it easier.


== List of categories to choose from

You can select only __one category__!

* Administration
* Installation
* Managing software
* Upgrading

[NOTE]
====
List is far from complete! TBD: Extend list of categories
====

== List of tags to choose from

You can select __multipe tags__!

* 1st level tags, choose at least one:
** How-to
** Tutorial

* 2nd level tags, choose multiple, none if system wide
** Workstation
*** Gnome
*** KDE
*** XFCE
** Silverblue
** Kinoite
** Server
** CoreOS
** IoT

* 3rd level tags, optional, choose multiple

** Problem-solving / Troubleshooting
** Printing / Scanning
** SELinux

You are free to choose additional 3rd level tag(s) if none fits your contribution.

== Docs team needs your help

Help us with the issues, PRs, onboarding new contributors or keeping our stock of articles as a whole up to date. Thank you!
